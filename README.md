# NSYNC

The NSYNC protocol WIP

Defines:

| NAME | DESCRIPTION |
| --- | --- |
| `nsUID` | NSYNC UID |
| `nsH` | NSYNC Hash |
| `nsTS` | NSYNC timestamp |
| `getnsH(nsUID)` | function to retrieve the `nsH`for a given `nsUID` |

## The idea

To everything you make available on the internet, you add a UUID, a Universally Unique ID. Which is called a `nsUID` (NSYNC UID). And this goes down to every single paragraph you write on a blog, every image you put on Mastodon or twitter, every line in your source code.

## The result

Now. Every time you read a web page, you download a git reopsitory, you get the content and those UUIDs not only for the whole thing, but down to every block element, be it a paragraph, an image or a line of code. Nice. So over time you have loads and loads of `nsUID`.

## The Hash

Optionally calculate a defined hash over that piece of content, called the `nsH` - NSYNC Hash. Now you have

- the content
- the nsUID
- the nsH
- the timestanmp of when you generated the hash, which we call a `nsTS`

## NSYNC

Whenever you read a web page, download source code or pictures, you may get an `nsUID`. And you can calculate the `nsH`. And collect those in a key/value store.

## The consequences

If someone quotes a paragraph or image or line of code somewhere elese - you don't need to download it, if you have the content, the `nsUID` and the `nsH`. So all of a sudden you have a globally distributed CDN (Content Delivery Network) a la CloudFare.

You ask for an updated hash with `getnsH(nsUID)`. You will find out if that quoted content has changed, who claims it has and when (the `nsTS` timestamp). Depending on how much you trust that source, you can decide to add a visual marker (content might have changed) or create a diff (this seems to have changed) or just take the updated content (and new hash) wholesale.

## So what?

What we have just created is very lightweight, eventually consistent way of sharing content of any kind based on a P2P (peer to peer) trust system without ANY central authority.