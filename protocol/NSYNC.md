## THE NSYNC PROTOCOL 0.0.1-DRAFT

## nsH

The `nsh` is  tolerant hash (TBD) of the content referenced by a `nsUID`.

## nsUID

Every referenceable item has it's own `nsUID` (NSYNC UUID).

Possible candidate for `nsUID` definition UUID v7 or 8 in [draft-peabody-dispatch-new-uuid-format](https://datatracker.ietf.org/doc/html/draft-peabody-dispatch-new-uuid-format)

## nsTS

The `nsTS` is a timestamp with the date/time of the nsH calculation for a given nsUID. 

## getnsH Function

```
getnsH(nsUID)
````

Returns:

```
NACK - I've never seen that UUID, sorry.
nsH - I've seen that one! This is the nsH that I have!
nsTS (optional) - And as I've seen it, here
                  is the timestamp I consider
                  that content to be valid from.
```

